package nilus;

//import org.eclipse.persistence.jaxb.dynamic.DynamicJAXBContext;


import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.System;
import java.nio.charset.StandardCharsets;

// XML classes
import javax.xml.bind.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMResult;

import org.w3c.dom.Document;

import nilus.NSMapper;

// These classes are required for unmarshalling XML to Java objects
import nilus.Detections;
import nilus.Deployment;
import nilus.Localize;
import nilus.Calibration;
import nilus.Ensemble;


//http://www.eclipse.org/eclipselink/documentation/2.4/moxy/dynamic_jaxb003.htm#BGBDCIBC

// query set by xpath
// http://www.eclipse.org/eclipselink/documentation/2.4/moxy/runtime008.htm

// moxy setValueByXpath man page
// http://www.eclipse.org/eclipselink/api/2.6/org/eclipse/persistence/jaxb/JAXBContext.html#setValueByXPath(java.lang.Object,%20java.lang.String,%20org.eclipse.persistence.oxm.NamespaceResolver,%20java.lang.Object)


/**
 * This class is used to convert Java architecture for XML binding (JAXB)
 * objects to XML.  
 * 
 * @author Sean Herbert and Marie Roch
 *
 */
public class MarshalXML {
	public static String schema = "http://tethys.sdsu.edu/schema/1.0";
	public static String schemaLoc = schema + " tethys.xsd";
	
	// JAXB context and marshaller objects
	protected JAXBContext jxbc;
	protected Marshaller jxbm;
	
	/**
	 * Nilus Marshaller class for Tethys w default schema location
	 * @throws JAXBException  Unable to marshal
	 */
	public MarshalXML() throws JAXBException {
		initMarshalXML(schemaLoc);
	}


	/**
	 * Nilus Marshaler class for Tehtys with specific schema location
	 * @param loc - Use null or "" for no location
	 * @throws JAXBException  Unable to marshal
	 */
	public MarshalXML(String loc) throws JAXBException {
		initMarshalXML(loc);
	}

	/**
	 * Initialize the XML
	 * @param loc Schema specification/location
	 * @throws JAXBException
	 */
	private void initMarshalXML(String loc) throws JAXBException { 	
		jxbc = JAXBContext.newInstance("nilus");  // Package with which JAXB will work
		jxbm = jxbc.createMarshaller();
		jxbm.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jxbm.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSMapper());
		if (loc != null && loc.length() > 0) {
			jxbm.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, loc);
		}
	}

	/**
	 * Marshal object to XML that will be stored in file
	 * 
	 * @param jaxb - Object to be marshaled
	 * @param filename - name of file to be written
	 * @throws FileNotFoundException  Unable to open file for output
	 * @throws JAXBException  Erorr marshaling to XML
	 */
	public void marshal(Object jaxb, String filename) throws FileNotFoundException, JAXBException {
		PrintWriter out = new PrintWriter(filename);
		marshal(jaxb, out);		
	}
	
	/**
	 * Marshal an object to XML contained in a String.
	 * 
	 * @param jaxb - Object to be marshaled
	 * @return String
	 * @throws JAXBException	Error marshaling to XML
	 */
	public String marshal(Object jaxb) throws JAXBException {
		StringWriter out = new StringWriter();
		jxbm.marshal(jaxb, out);
		String xml = out.toString();
		return xml;
	}
	
	/**
	 * Marshal an object to XML, writing out the result to a PrintWriter
	 * output stream.
	 * 
	 * @param jaxb - Object to be marshalled
	 * @param out - Writer stream to which object should be marshalled
	 * @throws JAXBException	Erorr marshaling to XML
	 */
	public void marshal(Object jaxb, PrintWriter out) throws JAXBException {		
		jxbm.marshal(jaxb, out);
	}
	
	/**
	 * Marshal an object to XML, writing out the result to an OutputStream
	 * or one of its derived classes
	 * 
	 * @param jaxb - Object to be marshalled
	 * @param out - java.io.OutpuStrream to which object should be marshalled
	 * @throws JAXBException	Error marshaling to XML
	 */
	public void marshal(Object jaxb, OutputStream out) throws JAXBException {		
		jxbm.marshal(jaxb, out);
	}
	/**
	 * Marshal an object to a DOM document with a single element which is the 
	 * marshalled representation of the JAXB object
	 * @param jaxb  JAX object to be marshaled to DOM
	 * @return	Resulting DOM Document
	 * @throws JAXBException  Unable to marshal
	 * @throws ParserConfigurationException Internal problem
	 */
	public Document marshalToDOM(Object jaxb) throws JAXBException, ParserConfigurationException {
		
		// Create a document with a single node containing the JAXB element
		Document document =
			    DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		DOMResult dom = new DOMResult(document);		
		jxbm.marshal(jaxb, dom);
		return document;
	}
	
	/**
	 * createInstance 
	 * Create an instance of a Tethys schema class.
	 * In general, this should be done using a top level schema class.  For example,
	 * to create a detections document:
	 * 		instance = MarshalXML.createInstance(nilus.Detections.class);
	 * 
	 * @param c  Create an instance of the specified schema class (Tethys
	 * 			 schema object
	 * @return Representation of object as JAXBContext
	 * @throws JAXBException  Unable to create the instance
	 */
	public JAXBContext createInstance(Class c) throws JAXBException {
		return JAXBContext.newInstance(c);
	}
	
	/**
	 * Given a string with a valid Deployment document, unmarshal to 
	 * plain-old Java objects (POJOs) generated by the JAXB compiler.
	 * 
	 * @param xml - UTF-8 string
	 * @return Deployment object
	 * @throws JAXBException
	 */
	public Deployment unmarshallDeployment(String xml) throws JAXBException {
		// Generate a context & deserializer specific to deployment instances
		JAXBContext context = JAXBContext.newInstance(Deployment.class);		
		Unmarshaller jaxb = context.createUnmarshaller();
		
		// Deserialize the XML to Java objects
		InputStream xml_stream = new ByteArrayInputStream(
				xml.getBytes(StandardCharsets.UTF_8));
		Deployment dep = (Deployment) jaxb.unmarshal(xml_stream);
		
		return dep;				
	}
}


