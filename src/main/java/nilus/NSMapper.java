package nilus;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class NSMapper extends NamespacePrefixMapper {
	private static final String TY_PREFIX = "";  // Default namespace
	private static final String TY_URI = "http://tethys.sdsu.edu/schema/1.0";
	
	/**
	 *  Map given Uri to a prefix
	 *  Use suggested URI if we don't know about it. 
	 */
	@Override
	public String getPreferredPrefix(
			String namespaceUri, 
			String suggestion,
			boolean requirePrefix) {
		
		String result;
		// Both xsi and http://tethys.sdsu.edu/schema/1.0 should
		// be mapped to the default namespace.
		if (TY_URI.equals(namespaceUri)) 
			result = TY_PREFIX;			
		else if (namespaceUri != null && namespaceUri.contains(TY_URI))
			result = TY_PREFIX;				
		else
			result = suggestion;
			
		return result;
	}
	
	@Override
	/**
	 * Return array of namespaces we are responsible for.
	 */
	public String[] getPreDeclaredNamespaceUris() {
		return new String[] { TY_URI };
	}
}
