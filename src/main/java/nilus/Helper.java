package nilus;

import java.io.IOException;
import java.lang.reflect.*;
import java.lang.Double;
import java.lang.String;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.XMLConstants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import nilus.MarshalXML;

/**
 * Class for creating required elements of a schema
 * @author mroch
 *
 */
public class Helper {
	
	protected nilus.MarshalXML marshaller;
	
	public Helper() throws JAXBException {
		marshaller = new MarshalXML(null);
	}
	
	/**
	 * Returns the version of Nilus as a string
	 * @return Nilus library version string
	 */
	public static String getVersion() {
		String ver;
		
		// Load project properties file
		final Properties props = new Properties();
		try {
			props.load(Helper.class.getClassLoader().getResourceAsStream("project.properties"));
			ver = props.getProperty("version");
		} catch (IOException e) {
			ver = "Unable to acess project.properties file or find version within";
		}
		// Return version from properties
		return ver;
	}
	
	/**
	 * Finds and creates required elements within an JAXB schema representation
	 * @param o Required elements will be created for this object
	 * @throws IllegalArgumentException illegal argument
	 * @throws IllegalAccessException illegal access
	 * @throws InstantiationException unable to instantiate
	 */
	public static void createRequiredElements(Object o) throws 
	IllegalArgumentException, IllegalAccessException, InstantiationException {

		// Find out what fields the object has
		Class<?> oclass = o.getClass();
		// Get list of all declared fields, even if not public
		Field[] fields = oclass.getDeclaredFields();

		// Step through the field and find which ones have
		// XmlElement annotations that are required
		for (Field f : fields) {
			if (f.isAnnotationPresent(XmlElement.class)) {
				XmlElement a = f.getAnnotation(XmlElement.class);
				if (a.required()) {
					if (f.get(o) == null) {
						// Create an instance for this variable if one is not already set
						Class<?> fclass = f.getType();
						if (hasDefaultConstructor(fclass)) {
							f.set(o, fclass.newInstance());
						
							// Process subentries
							createRequiredElements(f.get(o));
						}
					}					
				}				
			}
		}
	}
	
	/**
	 * Create specified element within a JAXB schema representation
	 * Any of the elements children that are required will also be created
	 * @param o Specified field will be created for this object
	 * @param field	Name of field to create
	 * @throws IllegalArgumentException  Bad argument
	 * @throws IllegalAccessException	Unable to access object
	 * @throws InstantiationException	Unable to instantiate
	 */
	public static void createElement(Object o, String field) throws 
	IllegalArgumentException, IllegalAccessException, InstantiationException {
		// Find out what fields the object has
		Class<?> oclass = o.getClass();
		// Get list of all declared fields, even if not public
		Field[] fields = oclass.getDeclaredFields();

		for (Field f : fields) {
			//XmlElement a = f.getAnnotation(XmlElement.class);
			if (f.getName().equalsIgnoreCase(field)) {
				if (f.get(o) == null) {
					// Create an instance for this variable if one is not already set
					Class<?> fclass = f.getType();
					if (hasDefaultConstructor(fclass)) {
						f.set(o, fclass.newInstance());
					
						// Process subentries, only creating required elements
						createRequiredElements(f.get(o));
					}
				}
				return;
			}				
		}
		// Did not find the field
		throw new IllegalArgumentException("Element is not in the schema for the parent");
	}
	
	/**
	 * Check if a class has a default constructor
	 * @param a_class Class to check
	 * @return  True implies default constructor exists
	 * 
	 */
	private static boolean hasDefaultConstructor(Class<?> a_class) {

		
		boolean noargs = false;
		
		if (a_class.toString().compareTo("class javax.xml.datatype.XMLGregorianCalendar") == 0) {
			/* XMLGregorianCalendar fails with exception InstantiationExceptionConstructorAccessorImpl when
			 * invoked from Class<T>.newinstance().  We treat as if there is not a default constructor 
			 */
			return false;
		}
		
		/*
		 * Code courtesy Sotirios Deliamanolis:
		 * https://stackoverflow.com/questions/27810634/how-can-i-check-a-class-has-no-arguments-constructor  
		 */
		for (Constructor<?> constructor : a_class.getConstructors()) {
			Class<?>[] argtypes = constructor.getParameterTypes();
			noargs = argtypes.length == 0;
			if (noargs) {
				break;
			}
		}
		return noargs;
		
	}
	
	/**
	 * Create a timestamp object suitable for setting any XML field with a type of xs:DateTime
	 * @param tstamp - Must be in format (ISO 8601 subset):
	 *   <a href="https://www.w3.org/TR/xmlschema-2/#dateTime-order">
	 *   	XML Schema 1.0 Part 2, Section 3.2.[7-14].1, Lexical Representation
	 *   </a>
	 * @return XMLGregorianCalendar
	 * @throws DatatypeConfigurationException problem with specified timestamp string
	 */
	public static XMLGregorianCalendar timestamp(String tstamp) throws DatatypeConfigurationException {
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(tstamp);		
	}

	/**
	 * Create a timestamp object suitable for setting any XML field with a type of xs:DateTime
	 * 
	 * @param year Year
	 * @param month	Month
	 * @param day	Day (numeric)
	 * @param hour	Hour of day (24 hour clock)
	 * @param minute	Minutes past hour
	 * @param second	Seconds past minute
	 * @param millisecond  Milliseconds past second
	 * @param timezone	Offset in minutes from UTC
	 * @return XMLGregorianCalendar
	 * @throws DatatypeConfigurationException Problem with data input values
	 */
	public static XMLGregorianCalendar timestamp(int year, int month, int day, int hour, int minute, 
			int second, int millisecond, int timezone) throws DatatypeConfigurationException {
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(
				year, month, day, hour, minute, second, millisecond, timezone);
	}
	
	/**
	 * Create a timestamp object suitable for setting any XML field with a type of xs:DateTime
	 * Use of BigDecimal allows arbitrary precision for fractional seconds.
	 * 
	 * @param year  Year
	 * @param month	Month (1-12)
	 * @param day	Day of month number
	 * @param hour	Hours into day
	 * @param minute	Minutes into hour
	 * @param second	Seconds into minute
	 * @param fractionalSecond	Fractions of a second past second
	 * @param timezone  Timezone offset (numeric)
	 * @return XMLGregorianCalendar
	 * @throws DatatypeConfigurationException Problem with time specification
	 */
	public static XMLGregorianCalendar timestamp(BigInteger year,
            int month,
            int day,
            int hour,
            int minute,
            int second,
            BigDecimal fractionalSecond,
            int timezone)
	throws  DatatypeConfigurationException {
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(
				year, month, day, hour, minute, second, fractionalSecond, timezone);
	}
	
	/**
	 * Given an element list, adds a new element and value to the list
	 * @param ellist  List to which we will add
	 * @param name	Name of element to add
	 * @param value	Value of element to add
	 * @return 
	 * @throws ParserConfigurationException Unable to parse
	 * @throws JAXBException 	Problem creating element
	 */
	public Element AddAnyElement(List<Element> ellist, String name, String value) 
			throws JAXBException, ParserConfigurationException {
		
		// Create a JAXB element and add it to a DOM list
		// QName qname = new QName(XMLConstants.DEFAULT_NS_PREFIX, name);
		QName qname = new QName(MarshalXML.schema, name, "ty");
		JAXBElement<String> jaxel = new JAXBElement<String>(
				qname, String.class, value);
		
		Document doc = marshaller.marshalToDOM(jaxel);  
		Element el = doc.getDocumentElement();
		ellist.add(el);
		return el;
	}
	
	
	/**
	 * XML xs:Integer type is implemented in JAXB as a BigInteger
	 * This routine converts a Java long to a BigInteger
	 * @param n  Integer to convert
	 * @return	BigInteger
	 */
	public static BigInteger toXsInteger(long n) {
		return BigInteger.valueOf(n);
	}
	
	/**
	 * XML xs:decimal type is implemented in JAXB as a BigDecimal
	 * This routine converts a Java double to a big decimal
	 * @param d double value to convert
	 * @return BigDecimal (xs:decimal)
	 */
	public static BigDecimal toXsDecimal(double d) {
		return BigDecimal.valueOf(d);
	}
	
	/**
	 * XML xs:double type is implemented in JAXB as a Double
	 * This routine converts a Java double to a Double
	 * @param d double value to convert
	 * @return Double object
	 */	public static Double toXsDouble(double d) {
		return Double.valueOf(d);
	}
	
}
