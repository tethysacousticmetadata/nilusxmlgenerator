<project xmlns="https://maven.apache.org/POM/4.0.0" xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="https://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>tethys</groupId>
  <artifactId>nilus</artifactId>
  <version>3.2</version>
  <packaging>jar</packaging>

  <name>nilus</name>
  <description>
  The Tethys metadata service can accept data in a variety of formats, but no
  conversion is required if data are submitted in XML that follow the Tethys
  metadata schemata.  Nilus is a library designed to allow bioacousticians to
  programmatically add Tethys compatible output to their detection, classification,
  and localization software.  Java objects are generated automatically from
  the Tethys schemata and can be accessed from any language that can
  interface with Java (e.g. Matlab, R).  In additional helper classes
  are included that provide capabilities such as marshalling data from
  a native data type to XML data types (e.g. timestamps), creating subelements,
  and generating the XML.
  </description>
  <url>https://maven.apache.org</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <jaxb.runtime.version>2.4.0-b180830.0438</jaxb.runtime.version>
    <jaxb.api.version>2.4.0-b180830.0359</jaxb.api.version>
    <jaxb.xjc.version>2.4.0-b180830.0438</jaxb.xjc.version>
  </properties>

  <dependencies>
    <!--
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
    -->
   
        <!--  2.6.2 is the last EclipseLink release to support Java 7
              2.7 requires Java 8
              Java 7 - Matlab 2013b - Matlab 2017a
              Java 8 is adopted in Matlab 2017b
          -->
    <dependency>
        <groupId>org.eclipse.persistence</groupId>
        <artifactId>org.eclipse.persistence.moxy</artifactId>
        <version>2.5.0</version>
    </dependency>
	<dependency>
	    <groupId>javax.xml.bind</groupId>
	    <artifactId>jaxb-api</artifactId>
	    <version>${jaxb.api.version}</version> 
	</dependency>
	<dependency>
	    <groupId>org.glassfish.jaxb</groupId>
	    <artifactId>jaxb-runtime</artifactId>
	    <version>${jaxb.runtime.version}</version> 
	</dependency>
	<dependency>
        <groupId>org.glassfish.jaxb</groupId>
        <artifactId>jaxb-xjc</artifactId>
        <version>${jaxb.xjc.version}</version>
    </dependency>
  </dependencies>
  <build>
	<resources>
		<resource>
			<!-- Have Maven expand variables in files in the resources directory -->
		    <directory>src/main/resources</directory>
		    <filtering>true</filtering>
		</resource>		
	</resources>
  	<plugins>
	    <plugin>    
	        <artifactId>maven-compiler-plugin</artifactId>
            <version>3.10.1</version>
	        <configuration>
	            <source>1.8</source>
	            <target>1.8</target>
	        </configuration>
	    </plugin>  	
  	    <plugin>
  	        <artifactId>maven-resources-plugin</artifactId>
            <version>3.0.2</version>
            <executions>
                <execution>
			        <id>copy-jaxb</id>
			        <phase>validate</phase>
			        <goals>
			          <goal>copy-resources</goal>
			        </goals>
			        <configuration>
			          <outputDirectory>${basedir}/target/client/nilus</outputDirectory>
			          <resources>
			            <resource>
			                <directory>src/main/resources</directory>
			                <filtering>true</filtering>
			            </resource>
			          </resources>
			        </configuration>
                </execution>
            </executions>
                   
  	    </plugin>
  		<plugin>
  			<groupId>org.jvnet.jaxb2.maven2</groupId>
  			<artifactId>maven-jaxb2-plugin</artifactId>
  			<version>0.14.0</version>
               <dependencies>
                    <dependency>
                        <groupId>org.glassfish.jaxb</groupId>
                        <artifactId>jaxb-runtime</artifactId>
                        <version>${jaxb.runtime.version}</version>
                    </dependency>
                </dependencies>  			
  			<executions>
      			<execution>
                        <id>generate</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                </execution>
            </executions>
            <configuration>
                <schemaDirectory>
                ${basedir}\..\DataRepository\coremeta\lib\schema
                </schemaDirectory>
                <schemaIncludes>
                    <schemaInclude>jaxb.xsd</schemaInclude>
                </schemaIncludes>
                <bindingDirectory>src/main/tethys/nilus</bindingDirectory>
                <generateDirectory>target/generated-sources/xjc</generateDirectory>
                <generatePackage>nilus</generatePackage>
            </configuration>
  		</plugin>
  		<plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <version>3.7.1</version>  
                <executions>
		          <execution>
		            <id>attach-descriptor</id>
		            <goals>
		              <goal>attach-descriptor</goal>
		            </goals>
		          </execution>
		        </executions>		
  		</plugin>
  	    <plugin>
	        	<groupId>org.apache.maven.plugins</groupId>
	       		<artifactId>maven-javadoc-plugin</artifactId>
	        	<version>3.1.0</version>
	        	<configuration>
	          		<show>private</show>
	          		<nohelp>true</nohelp>
	          		<sourcepath>
	          		${basedir}/src/main/java/nilus;
	          		${basedir}/target/generated-sources/xjc/nilus
	          		</sourcepath>
	        	</configuration>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>	        	
      	</plugin>
        <plugin>
          <version>3.0.0</version>
          <artifactId>maven-project-info-reports-plugin</artifactId>
          <groupId>org.apache.maven.plugins</groupId>
        </plugin>

  	</plugins>
  </build>
  <organization>
  	<name>Tethys</name>
  	<url>https://tethys.sdsu.edu</url>
  </organization>
</project>
